import Vue from 'vue'
import $ from 'jquery'

window.$ = window.jQuery = $

export default $

Vue.directive('ui-dropdown', {
  inserted (el, options) {
    $(el).dropdown(options)
  }
})
