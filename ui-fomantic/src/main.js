import Vue from 'vue'
import App from './App.vue'

import $ from './jquery-injection'
import 'fomantic-ui-css/semantic.min'
import 'fomantic-ui-css/semantic.min.css'

Vue.config.productionTip = false

Object.defineProperties(Vue.prototype, {
  $jquery: {
    get: () => $
  }
})

new Vue({
  render: h => h(App)
}).$mount('#app')
