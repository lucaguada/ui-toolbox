package io.cherrychain.ui.service;

import io.vertx.ext.web.handler.StaticHandler;

import static io.vertx.core.Vertx.vertx;
import static io.vertx.ext.web.Router.router;
import static java.lang.System.err;
import static java.lang.System.out;

public enum Main {;
  public static void main(String... args) {
    final var vertx = vertx();

    final var router = router(vertx);
    router.route("/*").handler(StaticHandler.create());
    vertx.createHttpServer()
      .requestHandler(router)
      .listen(8080)
      .onFailure(er -> err.format("UI Service can't start: %s", er.getMessage()))
      .onSuccess(it -> out.format("UI Service started on http://localhost:%d.\n", it.actualPort()));
  }
}
