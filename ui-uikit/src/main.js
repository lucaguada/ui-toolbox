import Vue from 'vue'
import App from './App.vue'

import UIkit from 'uikit/dist/js/uikit.min'
import Icons from 'uikit/dist/js/uikit-icons.min'
import 'uikit/dist/css/uikit.min.css'

Vue.config.productionTip = false

UIkit.use(Icons)

new Vue({
  render: h => h(App)
}).$mount('#app')
